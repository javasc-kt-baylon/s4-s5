package com.zuitt.example;

//Child class of Animal
    //extend keyword is used to inherit the properties and methods of the parent class.
public class Dog extends Animal {
    //properties
    private String breed;

    public Dog(){
        super();
        this.breed = "Chihuahua";

    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    //setter and getter
    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    //method
    public void speak(){
        System.out.println("Woof Woof!");
    }

    public void call(){
        //direct access with the parent method
        //super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog");
    }
}


//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/extends