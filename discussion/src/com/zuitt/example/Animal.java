package com.zuitt.example;

//Parent Class
public class Animal {
    //properties
    //private String name;
    //private String color;
    protected String name;
    protected String color;

    //constructor
    public Animal(){}

    //bakit hindi protected?? kapag magkaiba na silang package [PHP~~]
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    //setter and getter
    public String getName(){
        return this.name;
    }

    public String getColor(){
        return this.color;
    }

    //pwedeng igroup si set and get, pero naka group lang dito si setter and getter
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color){
        this.color = color;
    }

    //method
    public void call(){
        System.out.println("Hi my name is "+ this.name);
    }
}













